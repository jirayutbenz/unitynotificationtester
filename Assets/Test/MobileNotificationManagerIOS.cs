﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

#if UNITY_IOS
using Unity.Notifications.iOS;

public class MobileNotificationManagerIOS : MonoBehaviour
{
    public Text result;

    public string notificationId = "test_notificaiton";
    private int identifier;

    void Start()
    {
        iOSNotificationTimeIntervalTrigger timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(0, 0, 10),
            Repeats = false,
        };

        iOSNotificationCalendarTrigger calendarTrigger = new iOSNotificationCalendarTrigger()
        {
            Hour = 12,
            Minute = 0,
            Repeats = false
        };

        iOSNotificationLocationTrigger localtionTrigger = new iOSNotificationLocationTrigger()
        {
            Center = new Vector2(2.294498f, 48.858263f),
            Radius = 250f,
            NotifyOnEntry = true,
            NotifyOnExit = false,
        };

        iOSNotification notificaiton = new iOSNotification()
        {
            Identifier = "test_notification",
            Title = "Test Notification",
            Subtitle = "A Unity Royal Test Notification",
            Body = "This is a test notification",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notificaiton);

        iOSNotificationCenter.OnRemoteNotificationReceived += receiveNotification =>
        {
            Debug.Log("receive notification " + notificaiton.Identifier + " !");
            result.text = "Receive";
        };

        iOSNotification notificationIntentData = iOSNotificationCenter.GetLastRespondedNotification();

        if(notificationIntentData != null)
        {
            result.text = result.text + " data";
            Debug.Log("App was opened with notification");
        }
    }

    private void OnApplicationPause(bool pause)
    {
        iOSNotificationCenter.RemoveScheduledNotification(notificationId);
        iOSNotificationCenter.RemoveDeliveredNotification(notificationId);
    }



}
#endif